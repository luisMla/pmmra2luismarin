package com.luismla.game.charapters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.luismla.game.MainGame;

public class ShotAlien extends Shot {
	private static final float SPEED = 100;

	public ShotAlien(MainGame invaders, Vector2 posicion) {
		super(invaders, posicion);
		texturaShot = invaders.getManager().get("data/shotAlien.png", Texture.class);
		this.anchura = texturaShot.getWidth();
		this.altura = texturaShot.getHeight();
		bordes = new Rectangle(posicion.x, posicion.y, anchura, altura);
	}
	
	public void update(float delta) {
		posicion.y = posicion.y - SPEED * delta;
		bordes.y = posicion.y;
	}
	
	public void alienMuerto() {	}
}
