package com.luismla.game.bottons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.luismla.game.MainGame;

public class BotonConfiguration extends Boton{
    public BotonConfiguration(MainGame mainGame, Vector2 posicion) {
        super(mainGame, posicion);
        textura = mainGame.getManager().get("data/configuration.png",Texture.class);
        asignarBordes();
    }


    @Override
    protected void funcionamiento() {
        invaders.setScreen(invaders.CONFIGURACION);
    }
}
