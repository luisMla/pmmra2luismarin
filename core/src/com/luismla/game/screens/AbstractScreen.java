package com.luismla.game.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.luismla.game.MainGame;

public abstract class AbstractScreen implements Screen{
    protected MainGame mainGame;
    protected SpriteBatch batch;

    public AbstractScreen(MainGame mainGame) {
        this.mainGame = mainGame;
        this.batch = mainGame.getBatch();
    }

    public MainGame getMainGame() {
        return mainGame;
    }

    public void setMainGame(MainGame mainGame) {
        this.mainGame = mainGame;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }
}
